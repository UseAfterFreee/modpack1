import modsJson from './mods.json' assert { type: 'json' };
import { download, Destination } from "https://deno.land/x/download/mod.ts";

const GAME_ID = "432";
const MODS_CATEGORY = "6";

const get_req = (endpoint: string) => {
  const BASE_URL = 'https://api.curseforge.com';
  const API_KEY: &str = Deno.env.get("CURSE_API_KEY");

  let data = {
    headers: {
      "x-api-key": API_KEY
    }
  };

  // console.log(`${BASE_URL}${endpoint}`);
  return fetch(`${BASE_URL}${endpoint}`, data);
}

const mods: {fileId: number, name: string, modId: number}[] = modsJson;

const destination: Destination = {
  dir: './mods'
}

for (const mod of mods) {
  const get_url_req = `/v1/mods/${mod.modId}/files/${mod.fileId}/download-url`;
  const url = await get_req(get_url_req).then(resp => resp.json());

  console.log(`Downloading ${mod.name}...`);
  await download(url.data, destination);
}
