apt-get update && apt-get install -y wget unzip zip

wget https://github.com/denoland/deno/releases/download/v1.29.3/deno-x86_64-unknown-linux-gnu.zip
unzip deno-x86_64-unknown-linux-gnu.zip

mkdir mods

./deno run --allow-net --allow-write --allow-env download_mods.ts
zip mods.zip -r mods
